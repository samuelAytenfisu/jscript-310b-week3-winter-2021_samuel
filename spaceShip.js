// 1. Create a class function SpaceShip
// - should set two properties: name and topSpeed
// - should have a method accelerate that logs to the console 
//   `${name} moving to ${topSpeed}`

// SpaceShip
    class spaceShip {
          constructor(name, topSpeed) {
                this.name = name;
                this.topSpeed =  topSpeed;
      }
      accelerate() {
            const {name, topSpeed} = this;
            console.log(`${this.name} accelerating ${this.topSpeed}`)
      }
    }
    const spaceShipOne = new spaceShip('ATT','90 m/s');
    const spaceShipTwo = new spaceShip('Tmobile','9890 m/s');

spaceShipOne.accelerate();
spaceShipTwo.accelerate();

// 2. Call the constructor with a couple ships, 
// and call accelerate on both of them.
